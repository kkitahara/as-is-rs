use crate::{AsIs, BorrowAsIs, Is, IsCow, Owned, ToOwned};
use core::borrow::Borrow;

mod string;
pub use string::StringStub;

mod vec;
pub use vec::VecStub;

mod cow;

macro_rules! impl_for_copy_types {
    ($($T:ty),*$(,)?) => {
        $(
            impl BorrowAsIs for $T {
                type Is = $T;

                fn borrow_or_clone<B>(&self) -> IsCow<'_, B>
                where
                    Self::Is: Borrow<B>,
                    B: ?Sized + ToOwned<Owned = Owned<Self>>
                {
                    IsCow::Owned(*self)
                }
            }

            impl AsIs for $T {
                fn as_is<'a>(self) -> Is<'a, $T> {
                    Is::Owned(self)
                }
            }
        )*
    };
}

impl_for_copy_types!(
    i8, i16, i32, i64, i128, isize, u8, u16, u32, u64, u128, usize, f32, f64, bool,
);
