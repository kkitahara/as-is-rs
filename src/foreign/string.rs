use crate::BorrowAsIs;
use core::borrow::{Borrow, BorrowMut};
use core::cmp::Ordering;
use core::fmt;
use core::hash::{Hash, Hasher};
use core::str::{from_utf8, from_utf8_mut};

/// A stub for [`String`] used in `no_std` environments.
///
/// [`String`]: https://doc.rust-lang.org/alloc/string/struct.String.html
pub struct StringStub([u8; 0]);

impl StringStub {
    fn as_str(&self) -> &str {
        from_utf8(&self.0).ok().unwrap()
    }

    fn as_mut_str(&mut self) -> &mut str {
        from_utf8_mut(&mut self.0).ok().unwrap()
    }
}

impl fmt::Debug for StringStub {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.as_str().fmt(f)
    }
}

impl Borrow<str> for StringStub {
    fn borrow(&self) -> &str {
        self.as_str()
    }
}

impl BorrowMut<str> for StringStub {
    fn borrow_mut(&mut self) -> &mut str {
        self.as_mut_str()
    }
}

impl PartialEq for StringStub {
    fn eq(&self, other: &Self) -> bool {
        self.as_str().eq(other.as_str())
    }
}

impl Eq for StringStub {}

impl PartialOrd for StringStub {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for StringStub {
    fn cmp(&self, other: &Self) -> Ordering {
        self.as_str().cmp(other.as_str())
    }
}

impl Hash for StringStub {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.as_str().hash(state);
    }
}

impl BorrowAsIs for str {
    type Is = str;
}

#[cfg(not(feature = "alloc"))]
impl crate::ToOwned for str {
    type Owned = StringStub;
}

#[cfg(feature = "alloc")]
mod impl_for_string {
    use crate::{AsIs, BorrowAsIs, Is};
    use alloc::string::String;

    impl BorrowAsIs for String {
        type Is = String;
    }

    impl AsIs for String {
        fn as_is<'a>(self) -> Is<'a, Self> {
            Is::Owned(self)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use char_buf::CharBuf;
    use fmt::Write;
    use siphasher::sip::SipHasher;

    macro_rules! format {
        ($($arg:tt)*) => {
            {
                let mut w = CharBuf::<2>::new();
                write!(w, $($arg)*).unwrap();
                w
            }
        };
    }

    #[test]
    fn string_stub() {
        let mut s = StringStub([]);

        assert_eq!(Borrow::<str>::borrow(&s), "");
        assert_eq!(BorrowMut::<str>::borrow_mut(&mut s), "");
        assert_eq!(s < s, "" < "");
        assert_eq!(s == s, "" == "");
        assert_eq!(s.cmp(&s), "".cmp(&""));
        assert_eq!(format!("{:?}", s), "\"\"");

        let mut h1 = SipHasher::new();
        s.hash(&mut h1);

        let mut h2 = SipHasher::new();
        "".hash(&mut h2);

        assert_eq!(h1.finish(), h2.finish());
    }
}
