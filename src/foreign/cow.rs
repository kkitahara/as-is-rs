#![cfg(feature = "alloc")]

use crate::{AsIs, BorrowAsIs, Is, IsCow, Owned};
use alloc::borrow::{Borrow, Cow, ToOwned};

impl<T> BorrowAsIs for Cow<'_, T>
where
    T: ?Sized + BorrowAsIs<Is = T> + ToOwned,
{
    type Is = T;

    fn borrow_or_clone<B>(&self) -> IsCow<'_, B>
    where
        Self::Is: Borrow<B>,
        B: ?Sized + ToOwned<Owned = Owned<Self>>,
    {
        (**self).borrow_or_clone()
    }
}

impl<T> AsIs for Cow<'_, T>
where
    T: ?Sized + BorrowAsIs<Is = T> + ToOwned,
{
    fn as_is<'a>(self) -> Is<'a, T>
    where
        Self: 'a,
    {
        match self {
            Cow::Owned(x) => Is::Owned(x),
            Cow::Borrowed(x) => match x.borrow_or_clone() {
                IsCow::Owned(x) => Is::Owned(x),
                IsCow::Borrowed(_) => Is::Borrowed(x),
            },
        }
    }
}
