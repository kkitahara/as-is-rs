use crate::BorrowAsIs;
use core::borrow::{Borrow, BorrowMut};
use core::cmp::Ordering;
use core::fmt;
use core::hash::{Hash, Hasher};

/// A stub for [`Vec<T>`] used in `no_std` environments.
///
/// [`Vec<T>`]: https://doc.rust-lang.org/alloc/vec/struct.Vec.html
pub struct VecStub<T>([T; 0]);

impl<T> VecStub<T> {
    const fn as_slice(&self) -> &[T] {
        &self.0
    }

    fn as_mut_slice(&mut self) -> &mut [T] {
        &mut self.0
    }
}

impl<T> fmt::Debug for VecStub<T>
where
    T: fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.as_slice().fmt(f)
    }
}

impl<T> Borrow<[T]> for VecStub<T> {
    fn borrow(&self) -> &[T] {
        self.as_slice()
    }
}

impl<T> BorrowMut<[T]> for VecStub<T> {
    fn borrow_mut(&mut self) -> &mut [T] {
        self.as_mut_slice()
    }
}

impl<T> PartialEq for VecStub<T>
where
    T: PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        self.as_slice().eq(other.as_slice())
    }
}

impl<T> Eq for VecStub<T> where T: Eq {}

impl<T> PartialOrd for VecStub<T>
where
    T: PartialOrd,
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.as_slice().partial_cmp(other.as_slice())
    }
}

impl<T> Ord for VecStub<T>
where
    T: Ord,
{
    fn cmp(&self, other: &Self) -> Ordering {
        self.as_slice().cmp(other.as_slice())
    }
}

impl<T> Hash for VecStub<T>
where
    T: Hash,
{
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.as_slice().hash(state);
    }
}

impl<T> BorrowAsIs for [T]
where
    T: Clone,
{
    type Is = [T];
}

#[cfg(not(feature = "alloc"))]
impl<T> crate::ToOwned for [T]
where
    T: Clone,
{
    type Owned = VecStub<T>;
}

#[cfg(feature = "alloc")]
mod impl_for_vec {
    use crate::{AsIs, BorrowAsIs, Is};
    use alloc::vec::Vec;

    impl<T> BorrowAsIs for Vec<T>
    where
        T: Clone,
    {
        type Is = Vec<T>;
    }

    impl<T> AsIs for Vec<T>
    where
        T: Clone,
    {
        fn as_is<'a>(self) -> Is<'a, Self> {
            Is::Owned(self)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use char_buf::CharBuf;
    use fmt::Write;
    use siphasher::sip::SipHasher;

    macro_rules! format {
        ($($arg:tt)*) => {
            {
                let mut w = CharBuf::<2>::new();
                write!(w, $($arg)*).unwrap();
                w
            }
        };
    }

    #[test]
    fn vec_stub() {
        let mut v: VecStub<()> = VecStub([]);

        assert_eq!(v.borrow(), [(); 0]);
        assert_eq!(v.borrow_mut(), [(); 0]);
        assert_eq!(v < v, [(); 0] < [(); 0]);
        assert_eq!(v == v, [(); 0] == [(); 0]);
        assert_eq!(v.cmp(&v), [(); 0].cmp(&[(); 0]));
        assert_eq!(format!("{:?}", v), "[]");

        let mut h1 = SipHasher::new();
        v.hash(&mut h1);

        let mut h2 = SipHasher::new();
        [(); 0].hash(&mut h2);

        assert_eq!(h1.finish(), h2.finish());
    }
}
