use crate::{IsCow, IsMut};
use core::borrow::{Borrow, BorrowMut};

/// Immutably borrows the primary borrow target as [`Is`].
///
/// [`Is`]: trait.BorrowAsIs.html#associatedtype.Is
pub trait BorrowAsIs: Borrow<Self::Is> {
    /// The primary borrow target.
    type Is: ?Sized + BorrowAsIs<Is = Self::Is> + ToOwned;

    /// Immutably borrows from `self` as [`Is`].
    ///
    /// [`Is`]: trait.BorrowAsIs.html#associatedtype.Is
    fn borrow_as_is(&self) -> &Self::Is {
        self.borrow()
    }

    /// Immutably borrows from `self` by default, but can be overridden to clone `self` if
    /// appropriate.
    fn borrow_or_clone<B>(&self) -> IsCow<'_, B>
    where
        Self::Is: Borrow<B>,
        B: ?Sized + ToOwned<Owned = Owned<Self>>,
    {
        IsCow::Borrowed(self.borrow().borrow())
    }
}

impl<T> BorrowAsIs for &T
where
    Self: Borrow<T::Is>,
    T: ?Sized + BorrowAsIs,
{
    type Is = T::Is;

    fn borrow_or_clone<B>(&self) -> IsCow<'_, B>
    where
        Self::Is: Borrow<B>,
        B: ?Sized + ToOwned<Owned = Owned<Self>>,
    {
        (**self).borrow_or_clone()
    }
}

impl<T> BorrowAsIs for &mut T
where
    Self: Borrow<T::Is>,
    T: ?Sized + BorrowAsIs,
{
    type Is = T::Is;

    fn borrow_or_clone<B>(&self) -> IsCow<'_, B>
    where
        Self::Is: Borrow<B>,
        B: ?Sized + ToOwned<Owned = Owned<Self>>,
    {
        (**self).borrow_or_clone()
    }
}

/// Mutably borrows the primary borrow target as [`Is`].
///
/// [`Is`]: trait.BorrowAsIs.html#associatedtype.Is
pub trait BorrowMutAsIs: BorrowAsIs + BorrowMut<Self::Is> {
    /// Mutably borrows from `self` as [`Is`].
    ///
    /// [`Is`]: trait.BorrowAsIs.html#associatedtype.Is
    fn borrow_mut_as_is(&mut self) -> &mut Self::Is {
        self.borrow_mut()
    }

    /// Mutably borrows from `self` by default, but clones `self` if [`self.borrow_or_clone()`] is overridden to clone `self`.
    ///
    /// [`self.borrow_or_clone()`]: trait.BorrowAsIs.html#method.borrow_or_clone
    fn borrow_mut_or_clone<B>(&mut self) -> IsMut<'_, B>
    where
        Self::Is: BorrowMut<B>,
        B: ?Sized + ToOwned<Owned = Owned<Self>>,
    {
        match self.borrow_or_clone() {
            IsCow::Owned(x) => IsMut::Owned(x),
            IsCow::Borrowed(_) => IsMut::MutBorrowed(self.borrow_mut().borrow_mut()),
        }
    }
}

impl<T> BorrowMutAsIs for T where T: ?Sized + BorrowAsIs + BorrowMut<Self::Is> {}

#[doc(no_inline)]
#[cfg(feature = "alloc")]
pub use alloc::borrow::ToOwned;

/// An alternative to [`alloc::borrow::ToOwned`] available in a `no_std` environment.
///
/// Only associated type [`Owned`] is provided.
///
/// [`alloc::borrow::ToOwned`]: https://doc.rust-lang.org/alloc/borrow/trait.ToOwned.html
/// [`Owned`]: trait.ToOwned.html#associatedtype.Owned
#[cfg(not(feature = "alloc"))]
pub trait ToOwned {
    /// The resulting type after obtaining ownership.
    type Owned: Borrow<Self>;
}

#[cfg(not(feature = "alloc"))]
impl<T> ToOwned for T
where
    T: Clone,
{
    type Owned = T;
}

/// [`ToOwned`] types that can be mutably borrowed from [`Owned`].
///
#[cfg_attr(
    feature = "alloc",
    doc = "[`ToOwned`]: https://doc.rust-lang.org/alloc/borrow/trait.ToOwned.html"
)]
#[cfg_attr(
    feature = "alloc",
    doc = "[`Owned`]: https://doc.rust-lang.org/1.65.0/alloc/borrow/trait.ToOwned.html#associatedtype.Owned"
)]
#[cfg_attr(not(feature = "alloc"), doc = "[`ToOwned`]: trait.ToOwned.html")]
#[cfg_attr(
    not(feature = "alloc"),
    doc = "[`Owned`]: trait.ToOwned.html#associatedtype.Owned"
)]
pub trait ToOwnedMut: ToOwned<Owned = Self::OwnedMut> {
    /// The resulting type after obtaining ownership.
    type OwnedMut: BorrowMut<Self>;
}

impl<T> ToOwnedMut for T
where
    T: ?Sized + ToOwned,
    T::Owned: BorrowMut<T>,
{
    type OwnedMut = T::Owned;
}

/// The owned type associated through [`BorrowAsIs`] and [`ToOwned`].
///
/// [`BorrowAsIs`]: trait.BorrowAsIs.html
#[cfg_attr(
    feature = "alloc",
    doc = "[`ToOwned`]: https://doc.rust-lang.org/alloc/borrow/trait.ToOwned.html"
)]
#[cfg_attr(not(feature = "alloc"), doc = "[`ToOwned`]: trait.ToOwned.html")]
pub type Owned<T> = <<T as BorrowAsIs>::Is as ToOwned>::Owned;
