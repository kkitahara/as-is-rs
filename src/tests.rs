use crate::*;
#[cfg(feature = "alloc")]
use alloc::{borrow::Cow, vec};
use char_buf::CharBuf;
use core::fmt::Write;
use core::hash::{Hash, Hasher};
use core::ops::{
    Add, AddAssign, BitAnd, BitAndAssign, BitOr, BitOrAssign, BitXor, BitXorAssign, Div, DivAssign,
    Mul, MulAssign, Neg, Not, Rem, RemAssign, Shl, ShlAssign, Shr, ShrAssign, Sub, SubAssign,
};
use siphasher::sip::SipHasher;

#[derive(Clone)]
struct A<T>(T);

impl<T> BorrowAsIs for A<T>
where
    T: Clone,
{
    type Is = A<T>;
}

impl<T> AsIs for A<T>
where
    T: Clone,
{
    fn as_is<'a>(self) -> Is<'a, A<T>> {
        Is::Owned(self)
    }
}

macro_rules! impl_unop {
    ($Op:ident, $op:ident) => {
        impl<T> $Op for A<T>
        where
            T: $Op,
        {
            type Output = T::Output;

            fn $op(self) -> Self::Output {
                self.0.$op()
            }
        }

        impl<T> $Op for &A<T>
        where
            T: $Op + Clone,
        {
            type Output = T::Output;

            fn $op(self) -> Self::Output {
                self.clone().0.$op()
            }
        }

        impl<T> $Op for &mut A<T>
        where
            T: $Op + Clone,
        {
            type Output = T::Output;

            fn $op(self) -> Self::Output {
                self.clone().0.$op()
            }
        }
    };
}

impl_unop!(Neg, neg);
impl_unop!(Not, not);

macro_rules! impl_binop {
    ($Op:ident, $op:ident) => {
        impl<T, U> $Op<U> for A<T>
        where
            T: $Op + Clone,
            U: Borrow<A<T>>,
        {
            type Output = T::Output;

            fn $op(self, rhs: U) -> Self::Output {
                self.0.$op(rhs.borrow().clone().0)
            }
        }

        impl<T, U> $Op<U> for &A<T>
        where
            T: $Op + Clone,
            U: Borrow<A<T>>,
        {
            type Output = T::Output;

            fn $op(self, rhs: U) -> Self::Output {
                self.clone().0.$op(rhs.borrow().clone().0)
            }
        }

        impl<T, U> $Op<U> for &mut A<T>
        where
            T: $Op + Clone,
            U: Borrow<A<T>>,
        {
            type Output = T::Output;

            fn $op(self, rhs: U) -> Self::Output {
                self.clone().0.$op(rhs.borrow().clone().0)
            }
        }
    };
}

impl_binop!(Add, add);
impl_binop!(Sub, sub);
impl_binop!(Mul, mul);
impl_binop!(Div, div);
impl_binop!(Rem, rem);
impl_binop!(Shr, shr);
impl_binop!(Shl, shl);
impl_binop!(BitAnd, bitand);
impl_binop!(BitOr, bitor);
impl_binop!(BitXor, bitxor);

macro_rules! impl_binop_assign {
    ($OpAssign:ident, $op_assign:ident) => {
        impl<T, U> $OpAssign<U> for A<T>
        where
            T: $OpAssign + Clone,
            U: Borrow<A<T>>,
        {
            fn $op_assign(&mut self, rhs: U) {
                self.0.$op_assign(rhs.borrow().clone().0)
            }
        }
    };
}

impl_binop_assign!(AddAssign, add_assign);
impl_binop_assign!(SubAssign, sub_assign);
impl_binop_assign!(MulAssign, mul_assign);
impl_binop_assign!(DivAssign, div_assign);
impl_binop_assign!(RemAssign, rem_assign);
impl_binop_assign!(ShrAssign, shr_assign);
impl_binop_assign!(ShlAssign, shl_assign);
impl_binop_assign!(BitAndAssign, bitand_assign);
impl_binop_assign!(BitOrAssign, bitor_assign);
impl_binop_assign!(BitXorAssign, bitxor_assign);

fn is_owned<T: ?Sized + ToOwned>(x: Is<T>) -> bool {
    if let Is::Owned(_) = x {
        true
    } else {
        false
    }
}

fn is_borrowed<T: ?Sized + ToOwned>(x: Is<T>) -> bool {
    if let Is::Borrowed(_) = x {
        true
    } else {
        false
    }
}

fn is_mut_borrowed<T: ?Sized + ToOwned>(x: Is<T>) -> bool {
    if let Is::MutBorrowed(_) = x {
        true
    } else {
        false
    }
}

fn is_cow_owned<T: ?Sized + ToOwned>(x: IsCow<T>) -> bool {
    if let IsCow::Owned(_) = x {
        true
    } else {
        false
    }
}

fn is_cow_borrowed<T: ?Sized + ToOwned>(x: IsCow<T>) -> bool {
    if let IsCow::Borrowed(_) = x {
        true
    } else {
        false
    }
}

fn is_mut_owned<T: ?Sized + ToOwned>(x: IsMut<T>) -> bool {
    if let IsMut::Owned(_) = x {
        true
    } else {
        false
    }
}

fn is_mut_mut_borrowed<T: ?Sized + ToOwned>(x: IsMut<T>) -> bool {
    if let IsMut::MutBorrowed(_) = x {
        true
    } else {
        false
    }
}

fn check_borrow_or_clone<T>(x: T, f: fn(IsCow<T::Is>) -> bool) -> bool
where
    T: AsIs,
{
    f(x.borrow_or_clone())
}

#[test]
fn as_is() {
    let mut x = true;
    let mut y = [(); 1];
    let y = y.as_mut_slice();

    assert_eq!(is_owned(x.as_is()), true);
    assert_eq!(is_borrowed(x.as_is()), false);
    assert_eq!(is_mut_borrowed(x.as_is()), false);
    assert_eq!(is_owned((&x).as_is()), true);
    assert_eq!(is_borrowed((&x).as_is()), false);
    assert_eq!(is_mut_borrowed((&x).as_is()), false);
    assert_eq!(is_owned((&mut x).as_is()), true);
    assert_eq!(is_borrowed((&mut x).as_is()), false);
    assert_eq!(is_mut_borrowed((&mut x).as_is()), false);
    assert_eq!(is_owned(y.as_is()), false);

    assert_eq!(is_owned(x.into_is()), true);
    assert_eq!(is_owned((&x).into_is()), true);
    assert_eq!(is_owned((&mut x).into_is()), true);
    assert_eq!(is_owned((&*y).into_is()), false);
    assert_eq!(is_owned(y.into_is()), false);

    assert_eq!(is_cow_owned(x.into_is_cow()), true);
    assert_eq!(is_cow_borrowed(x.into_is_cow()), false);
    assert_eq!(is_cow_owned((&x).into_is_cow()), true);
    assert_eq!(is_cow_borrowed((&x).into_is_cow()), false);
    assert_eq!(is_cow_owned((&mut x).into_is_cow()), true);
    assert_eq!(is_cow_borrowed((&mut x).into_is_cow()), false);
    assert_eq!(is_cow_owned(y.into_is_cow()), false);
    assert_eq!(is_cow_borrowed((&*y).into_is_cow()), true);
    assert_eq!(is_cow_borrowed(y.into_is_cow()), true);

    assert_eq!(is_mut_owned(x.try_into_is_mut().ok().unwrap()), true);
    assert_eq!(
        is_mut_mut_borrowed(x.try_into_is_mut().ok().unwrap()),
        false
    );
    assert_eq!(is_mut_owned((&x).try_into_is_mut().ok().unwrap()), true);
    assert_eq!(is_mut_owned((&mut x).try_into_is_mut().ok().unwrap()), true);
    assert_eq!(is_mut_owned((&mut x).try_into_is_mut().ok().unwrap()), true);
    assert_eq!((&*y).try_into_is_mut().ok(), None);
    assert_eq!(is_mut_owned(y.try_into_is_mut().ok().unwrap()), false);

    assert_eq!(is_owned(x.try_into_owned().ok().unwrap().as_is()), true);
    assert_eq!(is_owned((&x).try_into_owned().ok().unwrap().as_is()), true);
    assert_eq!(
        is_owned((&mut x).try_into_owned().ok().unwrap().as_is()),
        true
    );
    assert_eq!((&*y).try_into_owned().ok(), None);
    assert_eq!(y.try_into_owned().ok(), None);

    assert_eq!(is_borrowed(y.borrow_as_is().as_is()), true);
    assert_eq!(is_mut_borrowed(y.borrow_mut_as_is().as_is()), true);

    assert_eq!(check_borrow_or_clone(x, is_cow_owned), true);
    assert_eq!(check_borrow_or_clone(&x, is_cow_owned), true);
    assert_eq!(check_borrow_or_clone(&mut x, is_cow_owned), true);

    assert_eq!(is_owned((&x.as_is()).as_is()), true);
    assert_eq!(is_borrowed((&(&*y).as_is()).as_is()), true);
    assert_eq!(is_borrowed((&y.as_is()).as_is()), true);

    assert_eq!(is_owned((&*x.into_is_cow()).as_is()), true);
    assert_eq!(is_borrowed((&*y.into_is_cow()).as_is()), true);

    assert_eq!(is_owned((&*x.into_is_mut()).as_is()), true);
    assert_eq!(is_borrowed((&*y.into_is_mut()).as_is()), true);
    assert_eq!(is_mut_borrowed((&mut *y.into_is_mut()).as_is()), true);

    assert_eq!(is_owned(1_i8.as_is()), true);
    assert_eq!(is_owned(1_i16.as_is()), true);
    assert_eq!(is_owned(1_i32.as_is()), true);
    assert_eq!(is_owned(1_i64.as_is()), true);
    assert_eq!(is_owned(1_i128.as_is()), true);
    assert_eq!(is_owned(1_isize.as_is()), true);
    assert_eq!(is_owned(1_u8.as_is()), true);
    assert_eq!(is_owned(1_u16.as_is()), true);
    assert_eq!(is_owned(1_u32.as_is()), true);
    assert_eq!(is_owned(1_u64.as_is()), true);
    assert_eq!(is_owned(1_u128.as_is()), true);
    assert_eq!(is_owned(1_usize.as_is()), true);
    assert_eq!(is_owned(1.0_f32.as_is()), true);
    assert_eq!(is_owned(1.0_f64.as_is()), true);

    assert_eq!(check_borrow_or_clone(1_i8, is_cow_owned), true);
    assert_eq!(check_borrow_or_clone(1_i16, is_cow_owned), true);
    assert_eq!(check_borrow_or_clone(1_i32, is_cow_owned), true);
    assert_eq!(check_borrow_or_clone(1_i64, is_cow_owned), true);
    assert_eq!(check_borrow_or_clone(1_i128, is_cow_owned), true);
    assert_eq!(check_borrow_or_clone(1_isize, is_cow_owned), true);
    assert_eq!(check_borrow_or_clone(1_u8, is_cow_owned), true);
    assert_eq!(check_borrow_or_clone(1_u16, is_cow_owned), true);
    assert_eq!(check_borrow_or_clone(1_u32, is_cow_owned), true);
    assert_eq!(check_borrow_or_clone(1_u64, is_cow_owned), true);
    assert_eq!(check_borrow_or_clone(1_u128, is_cow_owned), true);
    assert_eq!(check_borrow_or_clone(1_usize, is_cow_owned), true);
    assert_eq!(check_borrow_or_clone(1.0_f32, is_cow_owned), true);
    assert_eq!(check_borrow_or_clone(1.0_f64, is_cow_owned), true);
}

fn check_borrow_mut_or_clone<T>(mut x: T, f: fn(IsMut<T::Is>) -> bool) -> bool
where
    T: AsIsMut,
    Owned<T>: BorrowMut<T::Is>,
{
    f(x.borrow_mut_or_clone())
}

#[test]
fn as_is_mut() {
    let mut x = true;
    let mut y = [(); 1];
    let y = y.as_mut_slice();

    assert_eq!(is_mut_owned(x.as_is_mut()), true);
    assert_eq!(is_mut_mut_borrowed(x.as_is_mut()), false);
    assert_eq!(is_mut_owned((&mut x).as_is_mut()), true);
    assert_eq!(is_mut_mut_borrowed((&mut x).as_is_mut()), false);

    assert_eq!(is_mut_owned(x.into_is_mut()), true);
    assert_eq!(is_mut_owned((&mut x).into_is_mut()), true);
    assert_eq!(is_mut_owned(y.into_is_mut()), false);

    assert_eq!(check_borrow_mut_or_clone(x, is_mut_owned), true);
    assert_eq!(check_borrow_mut_or_clone(&mut x, is_mut_owned), true);
    assert_eq!(check_borrow_mut_or_clone(y, is_mut_mut_borrowed), true);

    assert_eq!(is_mut_owned(1_i8.as_is_mut()), true);
    assert_eq!(is_mut_owned(1_i16.as_is_mut()), true);
    assert_eq!(is_mut_owned(1_i32.as_is_mut()), true);
    assert_eq!(is_mut_owned(1_i64.as_is_mut()), true);
    assert_eq!(is_mut_owned(1_i128.as_is_mut()), true);
    assert_eq!(is_mut_owned(1_isize.as_is_mut()), true);
    assert_eq!(is_mut_owned(1_u8.as_is_mut()), true);
    assert_eq!(is_mut_owned(1_u16.as_is_mut()), true);
    assert_eq!(is_mut_owned(1_u32.as_is_mut()), true);
    assert_eq!(is_mut_owned(1_u64.as_is_mut()), true);
    assert_eq!(is_mut_owned(1_u128.as_is_mut()), true);
    assert_eq!(is_mut_owned(1_usize.as_is_mut()), true);
    assert_eq!(is_mut_owned(1.0_f32.as_is_mut()), true);
    assert_eq!(is_mut_owned(1.0_f64.as_is_mut()), true);
}

fn check_deref(x: &bool) -> bool {
    *x
}

fn check_deref_mut(x: &mut bool) -> bool {
    *x
}

#[test]
fn is() {
    let mut x = false;
    let mut y = [(); 1];
    let y = y.as_mut_slice();

    assert_eq!(is_owned(x.as_is().as_is()), true);
    assert_eq!(is_owned((&x).as_is().as_is()), true);
    assert_eq!(is_owned((&mut x).as_is().as_is()), true);
    assert_eq!(is_owned(Is::Borrowed(&x).as_is()), true);
    assert_eq!(is_owned(Is::MutBorrowed(&mut x).as_is()), true);
    assert_eq!(is_borrowed((&*y).as_is().as_is()), true);
    assert_eq!(is_mut_borrowed(y.as_is().as_is()), true);

    assert_eq!(is_owned((&x.as_is()).as_is()), true);
    // assert_eq!(is_mut_borrowed((&mut x.as_is()).as_is()), true);

    assert_eq!(check_borrow_or_clone(x.as_is(), is_cow_owned), true);
    assert_eq!(check_borrow_or_clone((&x).as_is(), is_cow_owned), true);
    assert_eq!(check_borrow_or_clone((&mut x).as_is(), is_cow_owned), true);

    assert_eq!(
        is_owned(<_ as Borrow<bool>>::borrow(&&x.as_is()).as_is()),
        true
    );
    assert_eq!(
        is_owned(<_ as Borrow<bool>>::borrow(&&mut x.as_is()).as_is()),
        true
    );

    assert_eq!(check_deref(&x.as_is()), x);
    assert_eq!(check_deref(&(&x).as_is()), x);
    assert_eq!(check_deref(&(&mut x).as_is()), x);

    let y = x.as_is();
    let z: &bool = y.borrow();
    assert_eq!(z, &x);

    assert_eq!(true.as_is() == false.as_is(), true == false);
    assert_eq!(true.as_is() == false.into_is_cow(), true == false);
    assert_eq!(true.as_is() == false.as_is_mut(), true == false);
    assert_eq!(1_i32.as_is() > 2_i32.as_is(), 1_i32 > 2_i32);
    assert_eq!(1_i32.as_is() > 2_i32.into_is_cow(), 1_i32 > 2_i32);
    assert_eq!(1_i32.as_is() > 2_i32.as_is_mut(), 1_i32 > 2_i32);
    assert_eq!(1_i32.as_is().cmp(&2_i32.as_is()), 1_i32.cmp(&2_i32));
    assert_eq!(Is::<bool>::default(), Is::Owned(bool::default()));

    assert_eq!(-(A(1_i32).as_is()), -1_i32);
    assert_eq!(-((&A(1_i32)).as_is()), -1_i32);
    assert_eq!(-((&mut A(1_i32)).as_is()), -1_i32);
    assert_eq!(!(A(true).as_is()), false);
    assert_eq!(!((&A(true)).as_is()), false);
    assert_eq!(!((&mut A(true)).as_is()), false);

    assert_eq!(-&(A(1_i32).as_is()), -1_i32);
    assert_eq!(-&((&A(1_i32)).as_is()), -1_i32);
    assert_eq!(-&((&mut A(1_i32)).as_is()), -1_i32);
    assert_eq!(!&(A(true).as_is()), false);
    assert_eq!(!&((&A(true)).as_is()), false);
    assert_eq!(!&((&mut A(true)).as_is()), false);

    assert_eq!(-&mut (A(1_i32).as_is()), -1_i32);
    assert_eq!(-&mut ((&A(1_i32)).as_is()), -1_i32);
    assert_eq!(-&mut ((&mut A(1_i32)).as_is()), -1_i32);
    assert_eq!(!&mut (A(true).as_is()), false);
    assert_eq!(!&mut ((&A(true)).as_is()), false);
    assert_eq!(!&mut ((&mut A(true)).as_is()), false);

    macro_rules! test_binop {
        ($a:literal.$op:ident($b:literal), $c:literal) => {
            assert_eq!((A($a).as_is()).$op(A($b)), $c);
            assert_eq!(((&A($a)).as_is()).$op(A($b)), $c);
            assert_eq!(((&mut A($a)).as_is()).$op(A($b)), $c);
            assert_eq!((A($a).as_is()).$op(&A($b)), $c);
            assert_eq!(((&A($a)).as_is()).$op(&A($b)), $c);
            assert_eq!(((&mut A($a)).as_is()).$op(&A($b)), $c);
            assert_eq!((A($a).as_is()).$op(&mut A($b)), $c);
            assert_eq!(((&A($a)).as_is()).$op(&mut A($b)), $c);
            assert_eq!(((&mut A($a)).as_is()).$op(&mut A($b)), $c);

            assert_eq!((&A($a).as_is()).$op(A($b)), $c);
            assert_eq!((&(&A($a)).as_is()).$op(A($b)), $c);
            assert_eq!((&(&mut A($a)).as_is()).$op(A($b)), $c);
            assert_eq!((&A($a).as_is()).$op(&A($b)), $c);
            assert_eq!((&(&A($a)).as_is()).$op(&A($b)), $c);
            assert_eq!((&(&mut A($a)).as_is()).$op(&A($b)), $c);
            assert_eq!((&A($a).as_is()).$op(&mut A($b)), $c);
            assert_eq!((&(&A($a)).as_is()).$op(&mut A($b)), $c);
            assert_eq!((&(&mut A($a)).as_is()).$op(&mut A($b)), $c);

            assert_eq!((&mut A($a).as_is()).$op(A($b)), $c);
            assert_eq!((&mut (&A($a)).as_is()).$op(A($b)), $c);
            assert_eq!((&mut (&mut A($a)).as_is()).$op(A($b)), $c);
            assert_eq!((&mut A($a).as_is()).$op(&A($b)), $c);
            assert_eq!((&mut (&A($a)).as_is()).$op(&A($b)), $c);
            assert_eq!((&mut (&mut A($a)).as_is()).$op(&A($b)), $c);
            assert_eq!((&mut A($a).as_is()).$op(&mut A($b)), $c);
            assert_eq!((&mut (&A($a)).as_is()).$op(&mut A($b)), $c);
            assert_eq!((&mut (&mut A($a)).as_is()).$op(&mut A($b)), $c);
        };
    }

    test_binop!(1_i32.add(1_i32), 2_i32);
    test_binop!(1_i32.sub(1_i32), 0_i32);
    test_binop!(1_i32.mul(1_i32), 1_i32);
    test_binop!(4_i32.div(2_i32), 2_i32);
    test_binop!(1_i32.rem(2_i32), 1_i32);
    test_binop!(2_u32.shr(1_u32), 1_u32);
    test_binop!(2_u32.shl(1_u32), 4_u32);
    test_binop!(2_u32.bitand(3_u32), 2_u32);
    test_binop!(2_u32.bitor(1_u32), 3_u32);
    test_binop!(3_u32.bitxor(1_u32), 2_u32);
}

#[test]
fn is_cow() {
    let mut x = false;
    let mut y = [(); 1];
    let y = y.as_mut_slice();

    assert_eq!(is_owned(x.into_is_cow().as_is()), true);
    assert_eq!(is_owned((&x).into_is_cow().as_is()), true);
    assert_eq!(is_owned((&mut x).into_is_cow().as_is()), true);
    assert_eq!(is_owned(IsCow::Borrowed(&x).as_is()), true);
    assert_eq!(is_borrowed((&*y).into_is_cow().as_is()), true);

    assert_eq!(is_owned((&x.into_is_cow()).as_is()), true);
    // assert_eq!(is_mut_borrowed((&mut x.into_is_cow()).as_is()), true);

    assert_eq!(check_borrow_or_clone(x.into_is_cow(), is_cow_owned), true);
    assert_eq!(
        check_borrow_or_clone((&x).into_is_cow(), is_cow_owned),
        true
    );
    assert_eq!(
        check_borrow_or_clone((&mut x).into_is_cow(), is_cow_owned),
        true
    );

    assert_eq!(
        is_owned(<_ as Borrow<bool>>::borrow(&&x.into_is_cow()).as_is()),
        true
    );
    assert_eq!(
        is_owned(<_ as Borrow<bool>>::borrow(&&mut x.into_is_cow()).as_is()),
        true
    );

    assert_eq!(check_deref(&x.into_is_cow()), x);
    assert_eq!(check_deref(&(&x).into_is_cow()), x);
    assert_eq!(check_deref(&(&mut x).into_is_cow()), x);

    let y = x.into_is_cow();
    let z: &bool = y.borrow();
    assert_eq!(z, &x);

    assert_eq!(true.into_is_cow() == false.as_is(), true == false);
    assert_eq!(true.into_is_cow() == false.into_is_cow(), true == false);
    assert_eq!(true.into_is_cow() == false.as_is_mut(), true == false);
    assert_eq!(1_i32.into_is_cow() > 2_i32.as_is(), 1_i32 > 2_i32);
    assert_eq!(1_i32.into_is_cow() > 2_i32.into_is_cow(), 1_i32 > 2_i32);
    assert_eq!(1_i32.into_is_cow() > 2_i32.as_is_mut(), 1_i32 > 2_i32);
    assert_eq!(
        1_i32.into_is_cow().cmp(&2_i32.into_is_cow()),
        1_i32.cmp(&2_i32)
    );
    assert_eq!(IsCow::<bool>::default(), IsCow::Owned(bool::default()));

    assert_eq!(-(A(1_i32).into_is_cow()), -1_i32);
    assert_eq!(-((&A(1_i32)).into_is_cow()), -1_i32);
    assert_eq!(!(A(true).into_is_cow()), false);
    assert_eq!(!((&A(true)).into_is_cow()), false);

    assert_eq!(-&(A(1_i32).into_is_cow()), -1_i32);
    assert_eq!(-&((&A(1_i32)).into_is_cow()), -1_i32);
    assert_eq!(!&(A(true).into_is_cow()), false);
    assert_eq!(!&((&A(true)).into_is_cow()), false);

    assert_eq!(-&mut (A(1_i32).into_is_cow()), -1_i32);
    assert_eq!(-&mut ((&A(1_i32)).into_is_cow()), -1_i32);
    assert_eq!(!&mut (A(true).into_is_cow()), false);
    assert_eq!(!&mut ((&A(true)).into_is_cow()), false);

    macro_rules! test_binop {
        ($a:literal.$op:ident($b:literal), $c:literal) => {
            assert_eq!((A($a).into_is_cow()).$op(A($b)), $c);
            assert_eq!(((&A($a)).into_is_cow()).$op(A($b)), $c);
            assert_eq!((A($a).into_is_cow()).$op(&A($b)), $c);
            assert_eq!(((&A($a)).into_is_cow()).$op(&A($b)), $c);
            assert_eq!((A($a).into_is_cow()).$op(&mut A($b)), $c);
            assert_eq!(((&A($a)).into_is_cow()).$op(&mut A($b)), $c);

            assert_eq!((&A($a).into_is_cow()).$op(A($b)), $c);
            assert_eq!((&(&A($a)).into_is_cow()).$op(A($b)), $c);
            assert_eq!((&A($a).into_is_cow()).$op(&A($b)), $c);
            assert_eq!((&(&A($a)).into_is_cow()).$op(&A($b)), $c);
            assert_eq!((&A($a).into_is_cow()).$op(&mut A($b)), $c);
            assert_eq!((&(&A($a)).into_is_cow()).$op(&mut A($b)), $c);

            assert_eq!((&mut A($a).into_is_cow()).$op(A($b)), $c);
            assert_eq!((&mut (&A($a)).into_is_cow()).$op(A($b)), $c);
            assert_eq!((&mut A($a).into_is_cow()).$op(&A($b)), $c);
            assert_eq!((&mut (&A($a)).into_is_cow()).$op(&A($b)), $c);
            assert_eq!((&mut A($a).into_is_cow()).$op(&mut A($b)), $c);
            assert_eq!((&mut (&A($a)).into_is_cow()).$op(&mut A($b)), $c);
        };
    }

    test_binop!(1_i32.add(1_i32), 2_i32);
    test_binop!(1_i32.sub(1_i32), 0_i32);
    test_binop!(1_i32.mul(1_i32), 1_i32);
    test_binop!(4_i32.div(2_i32), 2_i32);
    test_binop!(1_i32.rem(2_i32), 1_i32);
    test_binop!(2_u32.shr(1_u32), 1_u32);
    test_binop!(2_u32.shl(1_u32), 4_u32);
    test_binop!(2_u32.bitand(3_u32), 2_u32);
    test_binop!(2_u32.bitor(1_u32), 3_u32);
    test_binop!(3_u32.bitxor(1_u32), 2_u32);
}

#[test]
fn is_mut() {
    let mut x = false;
    let mut y = [(); 1];
    let y = y.as_mut_slice();

    assert_eq!(is_owned(x.as_is_mut().as_is()), true);
    assert_eq!(is_owned((&mut x).as_is_mut().as_is()), true);
    assert_eq!(is_owned(IsMut::MutBorrowed(&mut x).as_is()), true);
    assert_eq!(is_mut_borrowed(y.as_is_mut().as_is()), true);

    assert_eq!(is_owned((&x.as_is_mut()).as_is()), true);
    assert_eq!(is_owned((&mut x.as_is_mut()).as_is()), true);

    assert_eq!(is_mut_owned(x.as_is_mut().as_is_mut()), true);
    assert_eq!(is_mut_owned((&mut x).as_is_mut().as_is_mut()), true);

    assert_eq!(
        y.as_is_mut() == IsMut::MutBorrowed([(); 1].as_mut_slice()),
        true
    );

    assert_eq!(check_borrow_or_clone(x.as_is_mut(), is_cow_owned), true);
    assert_eq!(
        check_borrow_or_clone((&mut x).as_is_mut(), is_cow_owned),
        true
    );

    assert_eq!(
        is_owned(<_ as Borrow<bool>>::borrow(&&x.into_is_mut()).as_is()),
        true
    );
    assert_eq!(
        is_owned(<_ as Borrow<bool>>::borrow(&&mut x.into_is_mut()).as_is()),
        true
    );
    assert_eq!(
        is_owned(<_ as BorrowMut<bool>>::borrow_mut(&mut &mut x.into_is_mut()).as_is()),
        true
    );

    assert_eq!(check_deref(&x.as_is_mut()), x);
    assert_eq!(check_deref(&(&mut x).as_is_mut()), x);

    assert_eq!(check_deref_mut(&mut x.as_is_mut()), x);
    assert_eq!(check_deref_mut(&mut (&mut x).as_is_mut()), x);

    let y = x.as_is_mut();
    let z: &bool = y.borrow();
    assert_eq!(z, &x);

    let mut y = x.as_is_mut();
    let z: &mut bool = y.borrow_mut();
    assert_eq!(z, &mut x);

    assert_eq!(true.as_is_mut() == false.as_is(), true == false);
    assert_eq!(true.as_is_mut() == false.into_is_cow(), true == false);
    assert_eq!(true.as_is_mut() == false.as_is_mut(), true == false);
    assert_eq!(1_i32.as_is_mut() > 2_i32.as_is(), 1_i32 > 2_i32);
    assert_eq!(1_i32.as_is_mut() > 2_i32.into_is_cow(), 1_i32 > 2_i32);
    assert_eq!(1_i32.as_is_mut() > 2_i32.as_is_mut(), 1_i32 > 2_i32);
    assert_eq!(1_i32.as_is_mut().cmp(&2_i32.as_is_mut()), 1_i32.cmp(&2_i32));
    assert_eq!(IsMut::<bool>::default(), IsMut::Owned(bool::default()));

    assert_eq!(-(A(1_i32).as_is_mut()), -1_i32);
    assert_eq!(-((&mut A(1_i32)).as_is_mut()), -1_i32);
    assert_eq!(!(A(true).as_is_mut()), false);
    assert_eq!(!((&mut A(true)).as_is_mut()), false);

    assert_eq!(-&(A(1_i32).as_is_mut()), -1_i32);
    assert_eq!(-&((&mut A(1_i32)).as_is_mut()), -1_i32);
    assert_eq!(!&(A(true).as_is_mut()), false);
    assert_eq!(!&((&mut A(true)).as_is_mut()), false);

    assert_eq!(-&mut (A(1_i32).as_is_mut()), -1_i32);
    assert_eq!(-&mut ((&mut A(1_i32)).as_is_mut()), -1_i32);
    assert_eq!(!&mut (A(true).as_is_mut()), false);
    assert_eq!(!&mut ((&mut A(true)).as_is_mut()), false);

    macro_rules! test_binop {
        ($a:literal.$op:ident($b:literal), $c:literal) => {
            assert_eq!((A($a).as_is_mut()).$op(A($b)), $c);
            assert_eq!(((&mut A($a)).as_is_mut()).$op(A($b)), $c);
            assert_eq!((A($a).as_is_mut()).$op(&A($b)), $c);
            assert_eq!(((&mut A($a)).as_is_mut()).$op(&A($b)), $c);
            assert_eq!((A($a).as_is_mut()).$op(&mut A($b)), $c);
            assert_eq!(((&mut A($a)).as_is_mut()).$op(&mut A($b)), $c);

            assert_eq!((&A($a).as_is_mut()).$op(A($b)), $c);
            assert_eq!((&(&mut A($a)).as_is_mut()).$op(A($b)), $c);
            assert_eq!((&A($a).as_is_mut()).$op(&A($b)), $c);
            assert_eq!((&(&mut A($a)).as_is_mut()).$op(&A($b)), $c);
            assert_eq!((&A($a).as_is_mut()).$op(&mut A($b)), $c);
            assert_eq!((&(&mut A($a)).as_is_mut()).$op(&mut A($b)), $c);

            assert_eq!((&mut A($a).as_is_mut()).$op(A($b)), $c);
            assert_eq!((&mut (&mut A($a)).as_is_mut()).$op(A($b)), $c);
            assert_eq!((&mut A($a).as_is_mut()).$op(&A($b)), $c);
            assert_eq!((&mut (&mut A($a)).as_is_mut()).$op(&A($b)), $c);
            assert_eq!((&mut A($a).as_is_mut()).$op(&mut A($b)), $c);
            assert_eq!((&mut (&mut A($a)).as_is_mut()).$op(&mut A($b)), $c);
        };
    }

    test_binop!(1_i32.add(1_i32), 2_i32);
    test_binop!(1_i32.sub(1_i32), 0_i32);
    test_binop!(1_i32.mul(1_i32), 1_i32);
    test_binop!(4_i32.div(2_i32), 2_i32);
    test_binop!(1_i32.rem(2_i32), 1_i32);
    test_binop!(2_u32.shr(1_u32), 1_u32);
    test_binop!(2_u32.shl(1_u32), 4_u32);
    test_binop!(2_u32.bitand(3_u32), 2_u32);
    test_binop!(2_u32.bitor(1_u32), 3_u32);
    test_binop!(3_u32.bitxor(1_u32), 2_u32);

    macro_rules! test_binop_assign {
        ($a:literal.$op_assign:ident($b:literal), $c:literal) => {
            let mut x = A($a).clone().as_is_mut();
            x.$op_assign(A($b));
            assert_eq!((*x).0, $c);

            let mut x = A($a).clone().as_is_mut();
            x.$op_assign(&A($b));
            assert_eq!((*x).0, $c);

            let mut x = A($a).clone().as_is_mut();
            x.$op_assign(&mut A($b));
            assert_eq!((*x).0, $c);

            let mut x = A($a).clone();
            let mut y = (&mut x).as_is_mut();
            y.$op_assign(A($b));
            assert_eq!((*y).0, $c);

            let mut x = A($a).clone();
            let mut y = (&mut x).as_is_mut();
            y.$op_assign(&A($b));
            assert_eq!((*y).0, $c);

            let mut x = A($a).clone();
            let mut y = (&mut x).as_is_mut();
            y.$op_assign(&mut A($b));
            assert_eq!((*y).0, $c);
        };
    }

    test_binop_assign!(1_i32.add_assign(1_i32), 2_i32);
    test_binop_assign!(1_i32.sub_assign(1_i32), 0_i32);
    test_binop_assign!(1_i32.mul_assign(1_i32), 1_i32);
    test_binop_assign!(4_i32.div_assign(2_i32), 2_i32);
    test_binop_assign!(1_i32.rem_assign(2_i32), 1_i32);
    test_binop_assign!(2_u32.shr_assign(1_u32), 1_u32);
    test_binop_assign!(2_u32.shl_assign(1_u32), 4_u32);
    test_binop_assign!(2_u32.bitand_assign(3_u32), 2_u32);
    test_binop_assign!(2_u32.bitor_assign(1_u32), 3_u32);
    test_binop_assign!(3_u32.bitxor_assign(1_u32), 2_u32);
}

#[cfg(feature = "alloc")]
fn is_alloc_cow_owned<T>(x: Cow<T>) -> bool
where
    T: ?Sized + ToOwned,
{
    if let Cow::Owned(_) = x {
        true
    } else {
        false
    }
}

#[cfg(feature = "alloc")]
fn is_alloc_cow_borrowed<T>(x: Cow<T>) -> bool
where
    T: ?Sized + ToOwned,
{
    if let Cow::Borrowed(_) = x {
        true
    } else {
        false
    }
}

macro_rules! format {
    ($($arg:tt)*) => {
        {
            let mut w = CharBuf::<17>::new();
            write!(w, $($arg)*).unwrap();
            w
        }
    };
}

#[test]
fn debug() {
    assert_eq!(format!("{:?}", true.as_is()), "Owned(true)");
    assert_eq!(format!("{:?}", (&true).as_is()), "Owned(true)");
    assert_eq!(format!("{:?}", (&mut true).as_is()), "Owned(true)");
    assert_eq!(format!("{:?}", true.into_is_cow()), "Owned(true)");
    assert_eq!(format!("{:?}", (&true).into_is_cow()), "Owned(true)");
    assert_eq!(format!("{:?}", true.as_is_mut()), "Owned(true)");
    assert_eq!(format!("{:?}", (&mut true).as_is_mut()), "Owned(true)");
    assert_eq!(
        format!("{:?}", [(); 1].as_mut_slice().as_is_mut()),
        "MutBorrowed([()])"
    );
}

#[test]
#[cfg(feature = "alloc")]
fn cow() {
    let mut x = 1_i32;
    let mut y = [(); 1];
    let y = y.as_mut_slice();

    assert_eq!(is_alloc_cow_owned(x.into_cow()), true);
    assert_eq!(is_alloc_cow_borrowed(x.into_cow()), false);
    assert_eq!(is_alloc_cow_owned((&x).into_cow()), true);
    assert_eq!(is_alloc_cow_borrowed((&x).into_cow()), false);
    assert_eq!(is_alloc_cow_owned((&mut x).into_cow()), true);
    assert_eq!(is_alloc_cow_borrowed((&mut x).into_cow()), false);
    assert_eq!(is_alloc_cow_owned(y.into_cow()), false);
    assert_eq!(is_alloc_cow_borrowed((&*y).into_cow()), true);
    assert_eq!(is_alloc_cow_borrowed(y.into_cow()), true);

    assert_eq!(is_owned(x.into_cow().as_is()), true);
    assert_eq!(is_owned((&x).into_cow().as_is()), true);
    assert_eq!(is_borrowed(y.into_cow().as_is()), true);
    assert_eq!(is_owned(Cow::Borrowed(&x).as_is()), true);

    assert_eq!(check_borrow_or_clone(x.into_cow(), is_cow_owned), true);
    assert_eq!(check_borrow_or_clone((&x).into_cow(), is_cow_owned), true);
}

#[test]
#[cfg(feature = "alloc")]
fn string() {
    let mut s = "aaa".to_owned();

    assert_eq!(is_owned(s.clone().as_is()), true);
    assert_eq!(is_borrowed((&s).as_is()), true);
    assert_eq!(is_mut_borrowed((&mut s).as_is()), true);

    assert_eq!(is_mut_owned(s.clone().as_is_mut()), true);
    assert_eq!(is_mut_mut_borrowed((&mut s).as_is_mut()), true);
}

#[test]
#[cfg(feature = "alloc")]
fn vec() {
    let mut v = vec![(); 0];

    assert_eq!(is_owned(v.clone().as_is()), true);
    assert_eq!(is_borrowed((&v).as_is()), true);
    assert_eq!(is_mut_borrowed((&mut v).as_is()), true);

    assert_eq!(is_mut_owned(v.clone().as_is_mut()), true);
    assert_eq!(is_mut_mut_borrowed((&mut v).as_is_mut()), true);
}

#[test]
fn hash() {
    let mut h1 = SipHasher::new();
    true.as_is().hash(&mut h1);

    let mut h2 = SipHasher::new();
    true.hash(&mut h2);

    assert_eq!(h1.finish(), h2.finish());

    let mut h1 = SipHasher::new();
    true.into_is_cow().hash(&mut h1);

    assert_eq!(h1.finish(), h2.finish());

    let mut h1 = SipHasher::new();
    true.as_is_mut().hash(&mut h1);

    assert_eq!(h1.finish(), h2.finish());
}

#[derive(Clone)]
struct B(A<()>);

impl Borrow<A<()>> for &mut B {
    fn borrow(&self) -> &A<()> {
        &self.0
    }
}

impl BorrowMut<A<()>> for &mut B {
    fn borrow_mut(&mut self) -> &mut A<()> {
        &mut self.0
    }
}

impl BorrowAsIs for &mut B {
    type Is = A<()>;
}

impl AsIs for &mut B {
    fn as_is<'a>(self) -> Is<'a, A<()>>
    where
        Self: 'a,
    {
        Is::Borrowed(&self.0)
    }
}

#[test]
fn b() {
    let mut x = B(A(()));

    assert_eq!(is_borrowed((&mut x.clone()).borrow_as_is().as_is()), true);
    assert_eq!(is_mut_borrowed((&mut x).borrow_mut_as_is().as_is()), true);
}

#[test]
#[should_panic(expected = "Is::Borrowed variant returned from an AsIsMut type")]
fn panic() {
    let _ = B(A(())).as_is_mut();
}
