# Change log

## v0.0.30 - 2024.06.27
* Updated codes to adapt `clippy` warnings.

## v0.0.29 - 2023.10.06
* Simplified unit tests.

## v0.0.28 - 2023.10.05
* Added `documentation` field to `Cargo.toml`.
* Added unit tests.

## v0.0.27 - 2022.11.19
* No change (just for rebuild rustdoc).

## v0.0.26 - 2022.11.16
* Updated impls of `ops` traits for `Is*` to allow, e.g., `Is * Is`.

## v0.0.25 - 2022.11.11
* Separated `ref_ops` crate.

## v0.0.24 - 2022.11.09
* Entirely changed crate design.

## v0.0.23 - 2022.11.07
* Imploved impls of `AsIs` for `Is*`.

## v0.0.22 - 2022.11.07
* Changed blanket impls of `as_is::ToOwned`.

## v0.0.21 - 2022.11.07
* Largely changed impls of `ops` traits for `Is*`.
* Added `AsIsCow` trait.

## v0.0.20 - 2022.11.06
* Added impls of `AsIs` etc. for references of `Is*`.

## v0.0.19 - 2022.11.06
* Removed impls of `ops` traits from mutable references of `Is*` as these seemed surprising.

## v0.0.18 - 2022.11.06
* Added impls of ops traits for references of Is\*.
* Made *Op*Is and *RefOp*Is traits private because these seemed useless outside of this crate.

## v0.0.17 - 2022.11.02
* Separated BorrowIs (for unsized types) from AsIs (now, only for sized types).
* Changed some trait impls.

## v0.0.16 - 2022.11.01
* Changed trait bounds of ToOwned and AsIs to allow unsized types implement AsIs.

## v0.0.15 - 2022.10.16
* Added AsIsOwned trait.
* Added blanket impl for AsIsMut.

## v0.0.14 - 2022.10.16
* Removed redundant trait bound from AsIs::into\_is.

## v0.0.13 - 2022.10.16
* Minor changed for StringStub and VecStub.
* Changed src-tree.

## v0.0.12 - 2022.10.16
* Relaxed trait bound for ToOwned.

## v0.0.11 - 2022.10.15
* Changed some trait bounds.
* Integrated ref-ops crate with some modifications.

## v0.0.10 - 2022.10.10
* Changed some trait bounds.

## v0.0.9 - 2022.10.10
* Changed some trait bounds.

## v0.0.8 - 2022.10.10
* Removed OpAsIs traits.

## v0.0.7 - 2022.10.10
* Added OpIs traits.
* Changed some trait bounds.

## v0.0.6 - 2022.10.09
* Updated entirely.

## v0.0.5-alpha.2 - 2022.10.08
* Simplified docs.

## v0.0.5-alpha.1 - 2022.10.02
* Fixed docs.

## v0.0.5-alpha - 2022.10.02
* Updated entirely.

## v0.0.4-alpha - 2022.10.01
* Updated entirely; in particular, alloc feature is separated.

## v0.0.3-alpha - 2022.09.25
* Updated entirely.

## v0.0.2-alpha - 2022.09.24
* Updated entirely.

## v0.0.1-alpha.4 - 2020.10.05
* Removed useless trait bound from IsRef and IsRefMut.
* Updated documents.

## v0.0.1-alpha.3 - 2020.10.04
* Added IsRef and IsRefMut.
* Implemented *Op*Assign traits for IsMut.
* Updated documents.

## v0.0.1-alpha.2 - 2020.10.03
* Added IsCow, IsMut, IsOwned, and Owned.
* Implemented some common traits.

## v0.0.1-alpha - 2020.09.27
* Initial release.
