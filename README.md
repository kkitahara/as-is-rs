[![crates.io](https://img.shields.io/crates/v/as-is)](https://crates.io/crates/as-is)
[![docs.rs](https://img.shields.io/docsrs/as-is)](https://docs.rs/as-is)
[![pipeline status](https://gitlab.com/kkitahara/as-is-rs/badges/main/pipeline.svg)](https://gitlab.com/kkitahara/as-is-rs/-/pipelines)
[![coverage report](https://gitlab.com/kkitahara/as-is-rs/badges/main/coverage.svg)](https://kkitahara.gitlab.io/as-is-rs/coverage/)

# AsIs

**Development in progress**

Provides a trait `AsIs`, an enum `Is<'a, T>`, and variants of them.

## Todos

- [x] Unit tests
- [ ] Docs
- [ ] Examples

## Features

* alloc (default)

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.

---

&copy; 2020&ndash;2024 Koichi Kitahara
